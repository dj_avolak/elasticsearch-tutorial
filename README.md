# README #

### Instalacija ###

Nakon kloniranja repozitorijuma, potrebno je jednom jednostavnom komandom instalirati yii2 framework i ostale potrebne ekstenzije:
(ove komandu treba pokretati sa *root* direktorijuma aplikacije - *default* <webRoot>/elasticsearch-tutorial/)

```
#!cli

php composer.phar install --prefer-dist
```

Zatim, kada imamo sve potrebne komponente, mozemo da kreiramo indeks i podesimo mapiranje na ES-u.
Ukoliko je ES instaliran sa *default* podeshavanjima, ne bi trebalo da je potrebno bilo shta da se menja.
Za svaki sluchaj, ne shkodi proveriti konfiguracioni fajl : locations/config/params.php i parametre **elasticSearchHost** i **elasticSearchPort** .

```
#!cli

php locations/config/elasticSetup.php --insertTestItems
```

Potrebno je josh konfigurisati web server za yii framework, konfiguracije mozhete preuzeti sa vodica za yii instalaciju, i to za [Apache](http://www.yiiframework.com/doc-2.0/guide-start-installation.html#recommended-apache-configuration) kao i za [nginx](http://www.yiiframework.com/doc-2.0/guide-start-installation.html#recommended-nginx-configuration)

Na kraju otvaramo browser, adresa je -> <webRoot>/elasticsearch-tutorial/ ili <webRoot>/elasticsearch-tutorial/web/ zavisno od konfiguracije.