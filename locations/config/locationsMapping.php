<?php

return array(
    "name" => array(
        "type" => "string",
        "boost" => 1.3,
        "include_in_all" => true
    ),
    "description" => array(
        "type" => "multi_field",
        "fields" => array(
            "description" => array(
                "type" => "string",
                "index" => "analyzed",
                "boost" => 1.1
            ),
            "original" => array(
                "type" => "string",
                "index" => "not_analyzed",
                "include_in_all" => true
            )
        )
    ),
    "locationId" => array(
        "type" => "string",
        "include_in_all" => true
    ),
    "location" => array(
        "type" => "geo_point",
        "lat_lon" => true,
        "include_in_all" => false
    ),
    "isApproved" => array(
        "type" => "boolean",
        "include_in_all" => true
    )
);