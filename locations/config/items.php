<?php
return array(
    array(
        'locationId' => 1,
        'name' => 'pivo 1',
        'description' => 'majstor za pivo',
        'latitude' => 44.822216843094814,
        'longitude' => 20.47029733657837,
        'isApproved' => true
    ),
    array(
        'locationId' => 2,
        'name' => 'burger 1',
        'description' => 'kazhu dobar burger',
        'latitude' => 44.81532182870521,
        'longitude' => 20.460705757141113,
        'isApproved' => true
    ),
    array(
        'locationId' => 3,
        'name' => 'pica 1',
        'description' => 'pica sa sirom',
        'latitude' => 44.815154389845944,
        'longitude' => 20.460362434387207,
        'isApproved' => true
    ),
    array(
        'locationId' => 4,
        'name' => 'pica 2',
        'description' => 'pica na trgu',
        'latitude' => 44.81586980702732,
        'longitude' => 20.461628437042236,
        'isApproved' => true
    ),
    array(
        'locationId' => 5,
        'name' => 'pica 3',
        'description' => 'pica bucko',
        'latitude' => 44.81853351635622,
        'longitude' => 20.46431064605713,
        'isApproved' => true
    ),
    array(
        'locationId' => 6,
        'name' => 'pivo 2',
        'description' => 'samo pivo',
        'latitude' => 44.81163806149567,
        'longitude' => 20.4610276222229,
        'isApproved' => true
    ),
    array(
        'locationId' => 7,
        'name' => 'pivo 3',
        'description' => 'kornjachino pivo',
        'latitude' => 44.8143780760551,
        'longitude' => 20.46701431274414,
        'isApproved' => true
    ),
);