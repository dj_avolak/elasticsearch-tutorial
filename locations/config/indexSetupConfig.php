<?php

return array(
    'number_of_shards' => 2,
    'number_of_replicas' => 1,
    'analysis' => array(
        'filter' => array(
            'russianSnowball' => array(
                'type' => 'snowball',
                'language' => 'Russian'
            ),
            'customAsciiFolding' => array(
                'type' => 'asciifolding',
                'preserve_original' => true
            )
        ),
        'analyzer' => array(
            'indexAnalyzer' => array(
                'type' => 'custom',
                'tokenizer' => 'whitespace',
                'filter' => array('lowercase', 'customAsciiFolding')
            ),
            'searchAnalyzer' => array(
                'type' => 'custom',
                'tokenizer' => 'standard',
                'filter' => array('standard', 'lowercase', 'trim', 'customAsciiFolding')
            )
        ),
        'tokenizer' => array(
            'ngramTokenizer' => array(
                'type' => 'nGram',
                'min_gram' => 3,
                'max_gram' => 7,
                'token_chars' => array(
                    "letter"
                )
            )
        )
    )
);
