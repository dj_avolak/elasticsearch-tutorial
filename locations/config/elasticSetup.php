<?php

require(__DIR__ . '/../../vendor/autoload.php');

class ElasticSearchSetup
{
    const INDEX_CONFIG_PATH = 'indexSetupConfig.php';
    const MAPPING_CONFIG_PATH = 'indexMapping.php';

    /**
     * Will be used as index name.
     *
     * @var string
     */
    protected $applicationName = 'locations';

    /**
     * @var \Elastica\Client
     */
    protected $client;

    protected $errors;

    protected $proxy;

    /**
     * List of mapping files defines which types will be created. ['typeName' => 'filePath']
     *
     * @var array
     */
    protected $files = array(
        'mapping' => array(
            'locations' => 'locationsMapping.php',
        ),
        'items' => array(
            'startItems' => 'items.php'
        )
    );

    /**
     * Constructor method.
     */
    public function __construct()
    {
        $this->indexSetup = require_once(self::INDEX_CONFIG_PATH);

        $config = require_once('params.php');
        $this->config = $config;

        $hostVar = 'elasticSearchHost';
        $portVar = 'elasticSearchPort';
        if (!isset($this->config[$hostVar])) {
            throw new \Exception('no valid host found in configuration.');
        }
        $params = array(
            'host' => $this->config[$hostVar],
            'port' => $this->config[$portVar]
        );
        $this->client = new \Elastica\Client($params);
    }

    /**
     * Execute setup steps
     */
    public function setup($recreate = false, $insertTestItems = false)
    {
        try {
            /* @var \Elastica\Response $response */
            $response = $this->client->getIndex($this->applicationName)->create($this->indexSetup, $recreate);
            if ($response->isOk()) {
                echo 'Index for ' . $this->applicationName . ' created.' . PHP_EOL;
            } else {
                echo $response->getError();
                return;
            }
        } catch (\Exception $e) {
            if ($e->getMessage() == 'IndexAlreadyExistsException[[locations] already exists]') {
                echo 'Index for ' . $this->applicationName . ' not created, it already exists.' . PHP_EOL;
            } else {
                echo $e->getMessage();
            }
        }

        foreach ($this->files['mapping'] as $type => $fileName) {
            $this->setupMappings($type, $fileName);
        }

        if ($insertTestItems) {
            $this->indexItems('locations');
        }
    }

    /**
     * setup mappings from all files in config array
     */
    protected function setupMappings($type, $fileName)
    {
        $config = require_once($fileName);
        $mapping = new \Elastica\Type\Mapping();
        $mapping->setType($this->client->getIndex($this->applicationName)->getType($type));
        $mapping->setParam('index_analyzer', 'indexAnalyzer');
        $mapping->setParam('search_analyzer', 'searchAnalyzer');
        $mapping->setProperties($config);
        /* @var \Elastica\Response $response */
        $response = $mapping->send();
        if ($response->isOk()) {
            echo 'Mapping for ' . $type . ' created.' . PHP_EOL;
        } else {
            http_response_code(400);
            echo $response->getError();
            return;
        }
    }

    protected function indexItems($type)
    {
        $items = require_once($this->files['items']['startItems']);

        foreach ($items as $item) {
            $key = $item['locationId'];
            $item['location'] = $item['latitude'] . ',' . $item['longitude'];
            $document = new \Elastica\Document(
                $key,
                $item,
                $type,
                $this->applicationName
            );
            $response = $this->client->getIndex($this->applicationName)->getType($type)
                ->addDocument($document);
            if (!$response->isOk()) {
                $error = 'Failed to index item of type ' . $type
                    . ' and with ID : ' . $key . "\r\n"
                    . ' Error given by elastic : ' . $response->getError();
                echo $error;
                return false;
            }
        }
        $this->client->getIndex($this->applicationName)->refresh();
        return true;
    }
}

//supported options
$shortOptions = '';
$longOptions = array(
    'recreate::',
    'proxy::',
    'insertTestItems::'
);

//get params from command line
$params = getopt($shortOptions, $longOptions);

$recreate = false;
$proxy = false;
$insertTestItems = false;
if (isset($params['insertTestItems'])) {
    $insertTestItems = true;
}
if (isset($params['proxy']) && $params['proxy']) {
    $proxy = true;
}

$setup = new ElasticSearchSetup($proxy);

//perform setup
$setup->setup($recreate, $insertTestItems);