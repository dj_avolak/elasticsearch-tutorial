<?php

return [
    'adminEmail' => 'admin@example.com',
    'appName' => 'Locations',
    'elasticSearchHost' => 'localhost',
    'elasticSearchPort' => 9200,
];
