<?php

namespace app\models;

class Location extends \yii\base\Model
{
    /**
     * @var int
     */
    protected $locationId;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var float
     */
    protected $latitude;

    /**
     * @var bool
     */
    protected $isApproved;

    /**
     * @var float
     */
    protected $longitude;

    /**
     * @var \Elastica\Client
     */
    protected $client;

    /**
     * @var int
     */
    protected $distance;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'latitude', 'longitude'], 'required'],
            [['latitude', 'longitude'], 'number'],
//            [['name', 'description', 'latitude', 'longitude'], 'safe'],
            [['name', 'description'], 'string', 'max' => 255],
        ];
    }

    public function toArray(array $fields = array(), array $expand = array(), $recursive = true)
    {
        return array(
            'locationId' => $this->locationId,
            'description' => $this->description,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'name' => $this->name,
            'location' => $this->latitude . ',' . $this->longitude,
            'isApproved' => $this->isApproved,
        );
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param float $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @return int
     */
    public function getLocationId()
    {
        return $this->locationId;
    }

    /**
     * @param float $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $distance
     *
     * @return $this
     */
    public function setDistance($distance)
    {
        $this->distance = $distance;
        return $this;
    }

    /**
     * @param $status
     *
     * @return $this
     */
    public function setIsApproved($status)
    {
        $this->isApproved = $status;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsApproved()
    {
        return $this->isApproved;
    }

    /**
     *
     * @return int
     */
    public function getDistance()
    {
        if ($this->distance < 1000)  {
            return number_format($this->distance, 2) . 'm';
        } else {
            return number_format($this->distance / 1000, 2) . 'km';
        }
    }

    public function getPrimaryKey()
    {
        return $this->locationId;
    }

    /**
     * Tries to save data to indexer, and sets errors if operation fails
     *
     * @return bool
     */
    public function index()
    {
        $indexer = new \app\models\Indexer\Elastica\Index\LocationIndex($this->initElasticClient(), 'locations', $this);
        if (!$indexer->index()) {
            \Yii::$app->session->addFlash('app', $indexer->getError());

            return false;
        } else {
            \Yii::$app->session->addFlash('app', 'Location saved successfully');

            return true;
        }
    }

    public function delete()
    {
        $indexer = new \app\models\Indexer\Elastica\Index\LocationIndex($this->initElasticClient(), 'locations', $this);
        $indexer->delete();
        $this->client->optimizeAll();
    }

    /**
     * Initiates elastic search client if not already, and returns it
     *
     * @return \Elastica\Client
     */
    protected function initElasticClient()
    {
        if (!$this->client instanceof \Elastica\Client) {
            $config['servers'][] = array(
                'host' => \Yii::$app->params['elasticSearchHost'],
                'port' => \Yii::$app->params['elasticSearchPort'],
            );
            $config['logging'] = true;
            $this->client = new \Elastica\Client($config);
        }

        return $this->client;
    }

    /**
     * Returns locations from ES by given criteria. All defaults are taken care of in controller.
     *
     * @param string    $query
     * @param array     $latLng
     * @param string    $radius
     * @param int       $page
     * @param int       $perPage
     * @param null|bool $isApproved
     * @param string    $filter
     *
     * @return \ArrayObject
     */
    public function getLocations($query, $latLng, $radius, $page, $perPage, $isApproved = null, $filter)
    {
        //cover special filter case - by distance
        if ($filter === 'distance') {
            if (!strstr($query, 'm')) {
                $query .= 'km';
            }
            $radius = $query;
            //reset query
            $query = '';
        }

        $params = array(
            'query' => $query,
            'isApproved' => $isApproved,
            'indexName' => 'locations',
            'radius' => $radius,
            'filter' => $filter
        );

        if ((!$page) || ($page < 1)) {
            $page = 1;
        }

        if (!empty($latLng)) {
            $params['latitude'] = $latLng['latitude'];
            $params['longitude'] = $latLng['longitude'];
        }

        $search = new \app\models\Indexer\Elastica\Search\LocationSearch($this->initElasticClient(), $params);

        return $search->query($page, $perPage)->recreateObjectsFromResultSet();
    }

    public static function getLocation($id)
    {
        $config['servers'][] = array(
            'host' => \Yii::$app->params['elasticSearchHost'],
            'port' => \Yii::$app->params['elasticSearchPort'],
        );
        $config['logging'] = true;
        $client = new \Elastica\Client($config);
        $data = $client->getIndex('locations')->getType('locations')->getDocument($id);
        $model = new \app\models\Location();
        $model->setProperties($data->getData(), $data->getId());

        return $model;
    }

    public function setProperties($data, $locationId)
    {
        $this->description = $data['description'];
        $this->locationId = $locationId;
        $this->longitude = $data['longitude'];
        $this->latitude = $data['latitude'];
        $this->name = $data['name'];
        $this->isApproved = $data['isApproved'];
    }
} 