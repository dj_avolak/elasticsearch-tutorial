<?php
namespace app\models\Indexer\Elastica;

/**
 * Class AbstractIndexer
 *
 * @package app\models\Indexer\Elastica
 */
abstract class AbstractIndexer
{
    /**
     * index name on elastic search server, should be set in each child and is to be named same as child class
     *
     * @var string
     */
    protected $type = 'notSet';

    /**
     * Index (Indice) name
     *
     * @var string
     */
    protected $index;

    /**
     * Elasticsearch client object
     *
     * @var \Elastica\Client $client client object
     */
    protected $client;

    /**
     * Constructor method
     *
     * @param \Elastica\Client $client    elastic search client
     * @param string           $indexName elastic search index name
     */
    public function __construct(\Elastica\Client $client, $indexName)
    {
        $this->client = $client;
        $this->index = $indexName;
    }

    /**
     * Enables direct access to elastic search client
     *
     * @return \Elastica\Client
     */
    public function getElasticClientInstance()
    {
        return $this->client;
    }

    /**
     * Index name getter
     *
     * @return string
     */
    public function getIndexName()
    {
        return $this->index;
    }

    /**
     * Type name getter
     *
     * @return string
     */
    public function getTypeName()
    {
        return $this->type;
    }
}
