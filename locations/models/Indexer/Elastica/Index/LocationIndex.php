<?php
namespace app\models\Indexer\Elastica\Index;

/**
 * Class Index
 *
 * @package app\models\Indexer\Elastica\Index
 */
class LocationIndex extends \app\models\Indexer\Elastica\AbstractIndexer
{
    /**
     * Connected model object
     *
     * @var \app\models\Location $model
     */
    private $model;

    /**
     * ES type name
     * 
     * @var type 
     */
    protected $type = 'locations';

    /**
     * Error container
     * 
     * @var string
     */
    protected $error = '';

    /**
     * Constructor method
     *
     * @param \Elastica\Client       $client    elastic search client
     * @param string                 $indexName name of index to work with
     * @param \app\models\Location   $model     object to index
     */
    public function __construct(\Elastica\Client $client, $indexName, $model)
    {
        parent::__construct($client, $indexName);
        $this->model = $model;
    }

    /**
     * Delete item from search engine index
     *
     * @return void
     */
    public function delete()
    {
        $this->client->getIndex($this->getIndexName())->getType($this->getTypeName())
            ->deleteById($this->model->getPrimaryKey());
    }

    /**
     * Save item to search engine index
     *
     * @throws \app\models\Indexer\Elastica\Exception if indexing fails
     *
     * @return bool
     */
    public function index()
    {
        /* @var \Elastica\Document $document */
        $document = new \Elastica\Document(
            $this->model->getPrimaryKey(),
            $this->model->toArray(),
            $this->getTypeName(),
            $this->getIndexName()
        );

        $this->storeItems($document);
        //refresh elastic data
        $this->client->getIndex($this->getIndexName())->refresh();

        return ($this->error === '') ? true : false;
    }
    
    /**
     * Internal method that actualy stores items to ES.
     * 
     * @param type $document
     */
    protected function storeItems($document)
    {
        try {
            /* @var \Elastica\Bulk\ResponseSet $response */
            $response = $this->client->getIndex($this->getIndexName())->getType($this->getTypeName())
                ->addDocument($document);

            if (!$response->isOk()) {
                $this->setError($response->getError());
            }
        } catch (\Exception $e) {
            $this->setError($e->getMessage());
        }
    }

    /**
     * Error getter method.
     * 
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }
    
    /**
     * Error setter method.
     * 
     * @param string $sourceError error given by ES
     * 
     * @return void
     */
    protected function setError($sourceError)
    {
       $this->error = sprintf(
           'Failed to index item of type %s, with ID $s: Error given by ES $s: ', 
           get_class($this->model), 
           $this->model->getPrimaryKey(), 
           $sourceError
        ); 
    }
}
