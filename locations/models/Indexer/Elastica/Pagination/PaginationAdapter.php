<?php
namespace Backend\Model\Indexer\Elastica\Pagination;

class PaginationAdapter implements \Phalcon\Paginator\AdapterInterface
{
    /**
     * @var int $itemsPerPage how many items to display per page
     */
    protected $itemsPerPage;

    /**
     * @var int $page current page
     */
    protected $page;

    /**
     * Search class providing access to appropriate index via Elasticsearch Api
     *
     * @var \Backend\Model\Indexer\Elastica\Search\AbstractSearch $search
     */
    protected $search;

    /**
     * model used for search
     *
     * @var \Rasserbia\Mvc\Model
     */
    protected $modelInstance;

    /**
     * Constructor method
     *
     * @param array                                           $config        pagination configuration
     * @param \Backend\Model\Indexer\Elastica\AbstractIndexer $search        search model
     * @param mixed                                           $modelInstance instance of model which is used in search
     */
    public function __construct($config, \Backend\Model\Indexer\Elastica\AbstractIndexer $search, $modelInstance)
    {
        $this->itemsPerPage = $config['limit'];
        $this->page = $config['page'];
        $this->search = $search;
        $this->modelInstance = $modelInstance;
    }

    /**
     * Set the current page number
     *
     * @param int $page page number
     *
     * @return $this
     */
    public function setCurrentPage($page)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Getter for current page
     *
     * @return int
     */
    public function getCurrentPage()
    {
        return $this->page;
    }

    /**
     * Returns a slice of the result set to show in the pagination
     *
     * @return object stdClass prepared pagination object
     */
    public function getPaginate()
    {
        $rez = new \stdClass();
        $this->search->query($this->page, $this->itemsPerPage);
        $rez->items = $this->search->recreateObjectsFromResultSet($this->modelInstance);
        $rez->first = 1;
        $rez->current = $this->page;
        $rez->last = ceil($this->search->getTotalHits() / $this->itemsPerPage);
        $rez->next = $rez->current + 1;
        $rez->before = $rez->current - 1;
        $rez->total_items = $this->search->getTotalHits();
        $rez->total_pages = $rez->last;

        return $rez;
    }

    /**
     * Public access to search model
     *
     * @return \Backend\Model\Indexer\Elastica\Search\AbstractSearch
     */
    public function getSearchModel()
    {
        return $this->search;
    }
}
