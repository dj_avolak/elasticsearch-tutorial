<?php
namespace app\models\Indexer\Elastica\Search;

class LocationSearch extends \app\models\Indexer\Elastica\AbstractIndexer
{
    /**
     * Elastica query object
     *
     * @var \Elastica\Query $query
     */
    protected $query;

    /**
     * Total number of results for last query, defaults to 0
     *
     * @var int $totalHits
     */
    protected $totalHits = 0;

    /**
     * @var \Elastica\ResultSet $resultSet
     */
    protected $resultSet;

    /**
     * Array storage for returned items
     * @var \ArrayObject $items
     */
    protected $items;

    protected $type = 'locations';

    protected $radius;

    protected $latitude;

    protected $longitude;

    protected $input;

    /**
     * @var \Elastica\Search
     */
    protected $search;

    /**
     * @var \Elastica\QueryBuilder
     */
    protected $queryBuilder;

    /**
     * Constructor method.
     *
     * @param \Elastica\Client $client elastic search client
     * @param array            $params params
     */
    public function __construct(\Elastica\Client $client, $params)
    {
        parent::__construct($client, $params['indexName']);

        // setup query and query builder for ES version 1.3
        $this->query = new \Elastica\Query();
        $this->queryBuilder = new \Elastica\QueryBuilder(
            new \Elastica\QueryBuilder\Version\Version130()
        );
        $this->search = new \Elastica\Search($this->getElasticClientInstance());
        $this->setupSearch($params);
    }

    /**
     * Setup search parameters.
     * 
     * @param type $params
     */
    public function setupSearch($params) 
    {
        $this->search->addIndex($this->getIndexName());
        $this->search->addType($this->getTypeName());
        $qb = $this->queryBuilder;

        $filter = $qb->filter()->bool()->addMust(
            $qb->filter()->geo_distance(
                'location',
                array('lat' => $params['latitude'], 'lon' => $params['longitude']),
                $params['radius']
            )
        );

        if ($params['isApproved'] !== null) {
            $filter->addMust(
                $qb->filter()->term(['isApproved' => $params['isApproved']])
            );
        }

        $query = $qb->query()->match_all();
        if ($params['query'] !== '') {
            //allows for simple auto suggest with appending asterisk to string
            $query = $qb->query()->query_string()->setParams(array(
                'default_field' => $params['filter'],
                'query' => $params['query'] . '*',
            ));

            //more precise query, matches entire terms based on analyze setup
            /*$query = $qb->query()->match()->setParams(array(
                $params['filter'] => array(
                    'query' => $params['query'] . '*',
                    'operator' => 'and',
                    'fuzziness' => 1.5,
                )
            ));*/
        }

        $this->query->setQuery(
            $qb->query()->filtered(
                $query,
                $filter
            )
        );
        $sort = array(
            '_geo_distance' => array(
                'location' => array(
                    'lat' => $params['latitude'],
                    'lon' => $params['longitude'],
                ),
                'order' => 'asc',
                'unit' => 'm',
            )
        );
        $this->query->addSort($sort);
        $this->search->setQuery($this->query);
    }

    /**
     * Queries the search server with params for pagination
     *
     * @param int $page    current page
     * @param int $perPage items per page
     *
     * @return $this
     */
    public function query($page, $perPage)
    {
        $from = ($page - 1) * $perPage;
        $this->query->setFrom($from)->setSize($perPage);
        $this->setResultSet($this->search->search());
        $this->totalHits = $this->getResultSet()->getTotalHits();

        return $this;
    }

    /**
     * Recreates new instances of each object based on data returned from search engine
     *
     * @return \ArrayObject array of model instances
     */
    public function recreateObjectsFromResultSet()
    {
        $results = new \ArrayObject();

        foreach ($this->getResultSet()->getResults() as $objectData) {
            $location = new \app\models\Location();
            $location->setProperties($objectData->getData(), $objectData->getId());
            $location->setLatitude(explode(',', $objectData->getData()['location'])[0]);
            $location->setLongitude(explode(',', $objectData->getData()['location'])[1]);
            $location->setDistance($objectData->getParam('sort')[0]);

            $results->append($location);
        }

        return $results;
    }

    /**
     * Getter for query
     *
     * @return \Elastica\Query|object
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * Setter for query
     *
     * @param \Elastica\Query $query query object
     *
     * @return void
     */
    public function setQuery($query)
    {
        $this->query = $query;
    }

    /**
     * Getter for total results, 0 default
     *
     * @return int
     */
    public function getTotalHits()
    {
        return $this->totalHits;
    }

    /**
     * Setter for totalHits, cannot set number less than zero.
     *
     * @param int $count total results
     *
     * @return void
     */
    public function setTotalHits($count)
    {
        if ($count < 0) {
            $count = 0;
        }

        $this->totalHits = $count;
    }

    /**
     * Setter for resultSet
     *
     * @param \Elastica\ResultSet $resultSet result set
     *
     * @return void
     */
    public function setResultSet($resultSet)
    {
        $this->resultSet = $resultSet;
    }

    /**
     * Getter for resultSet
     *
     * @return \Elastica\ResultSet
     */
    public function getResultSet()
    {
        return $this->resultSet;
    }

    /**
     * Prints out current query as JSON formatted string.
     *
     * @return string
     */
    public function getQueryAsJson()
    {
        return json_encode($this->getQuery()->toArray());
    }
}
