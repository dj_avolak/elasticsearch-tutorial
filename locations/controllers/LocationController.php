<?php

namespace app\controllers;

class LocationController extends \yii\web\Controller
{
    const DEFAULT_RADIUS = "5km";
    const DEFAULT_LAT = 44.8160;
    const DEFAULT_LONG = 20.4602;
    const DEFAULT_PER_PAGE = 10;
    const DEFAULT_FILTER = '_all';

    /**
     * Action that deals with creation of an item.
     * 
     * @return string
     */
    public function actionCreate()
    {
        $model = new \app\models\Location();

        if (\Yii::$app->request->isPost) {
            $model->load(\Yii::$app->request->post());
            $model->setIsApproved(false);
            if ($model->index()) {
                $this->redirect('/location/index');
            }
        }

        return $this->render('create', array('model' => $model));
    }

    /**
     * Action that deals with displaying items.
     * 
     * @return string
     */
    public function actionIndex()
    {
        $model = new \app\models\Location();

        $latLng = array(
            'latitude' => self::DEFAULT_LAT,
            'longitude' => self::DEFAULT_LONG
        );
        if (\Yii::$app->request->get('latitude', false)) {
            $latLng['latitude'] = \Yii::$app->request->get('latitude', false);
        }
        if (\Yii::$app->request->get('longitude', false)) {
            $latLng['longitude'] = \Yii::$app->request->get('longitude', false);
        }
        // set all default values here
        $locations = $model->getLocations(
            \Yii::$app->request->get('query', ''),
            $latLng,
            \Yii::$app->request->get('radius', self::DEFAULT_RADIUS),
            \Yii::$app->request->get('page', 1),
            \Yii::$app->request->get('perPage', self::DEFAULT_PER_PAGE),
            \Yii::$app->request->get('isApproved', null),
            \Yii::$app->request->get('filter', self::DEFAULT_FILTER)
        );

        return $this->render('index', array(
            'locations' => $locations
        ));
    }

    /**
     * Action that deals with updating of an item.
     * 
     * @return string
     */
    public function actionUpdate()
    {
        $model = \app\models\Location::getLocation(\Yii::$app->request->get('locationId', null));

        if (\Yii::$app->request->isPost) {
            $model->load(\Yii::$app->request->post());
            $model->setIsApproved(true);
            if ($model->index()) {
                $this->redirect('/location/index');
            }
        }

        return $this->render('update', array('model' => $model));
    }

    /**
     * Action that deals with deleting an item.
     * 
     * @return Response
     */
    public function actionDelete()
    {
        $model = \app\models\Location::getLocation(\Yii::$app->request->get('locationId', null));
        $model->delete();
        \Yii::$app->session->addFlash('app', 'Item deleted successfully');
        $this->redirect('/location/index');
    }
}
