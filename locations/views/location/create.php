<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \app\models\Location */

$this->title = 'Create location';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Locations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="location-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('form', [
            'model' => $model,
        ]) ?>

</div>
