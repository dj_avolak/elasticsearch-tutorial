<?php
/* @var $this yii\web\View */
/* @var $location app\models\Location */
/* @var $locations \ArrayObject */
?>
<h1>View locations</h1>


<label for="filter">Search by all fields:</label>
<input size="10" type="text" id="filter" />
<table>
    <tr>
        <th width="30">#</th>
        <th width="120"><label for="name">Name</label></th>
        <th width="200"><label for="description">Description</label></th>
        <!--<th width="170">Position</th>-->
        <th width="150"><label for="distance">Distance</label></th>
        <th width="80"><label for="approved">approved</label></th>
        <th>actions</th>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td><input size="10" type="text" id="name" class="fieldFilter" /></td>
        <td><input size="10" type="text" id="description" class="fieldFilter" /></td>
        <td><input size="5" type="text" id="distance" class="fieldFilter" /></td>
        <td><input size="1" type="text" id="approved" class="fieldFilter" /></td>
        <td>&nbsp;</td>
    </tr>
    <?php foreach ($locations as $key => $location): ?>
    <tr>
        <td><?=$key+1?></td>
        <td><?=$location->getName()?></td>
        <td><?=$location->getDescription()?></td>
        <!--<td><?=$location->getLatitude()?>, <?=$location->getLongitude()?></td>-->
        <td><?=$location->getDistance()?></td>
        <td><?=$location->getIsApproved()?></td>
        <td>
            <a href="/location/delete?locationId=<?=$location->getLocationId()?>" title="Delete">Delete</a>
            <a href="/location/update?locationId=<?=$location->getLocationId()?>" title="Update">Update</a>
        </td>
    </tr>
    <?php endforeach; ?>
</table>

<script>
    $(document).ready(function() {
        $('.fieldFilter').keypress(function(e) {
            if (e.which === 13) {
                window.location = window.location.origin + window.location.pathname +
                    '?filter=' + $(this).attr('id') + '&query=' + $(this).val();
            }
        });
        $('#filter').keypress(function(e) {
            if (e.which === 13) {
                window.location = window.location.origin + window.location.pathname +
                    '?filter=_all&query=' + $(this).val();
            }
        });
    });
</script>