<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Locations */
/* @var $form ActiveForm */
?>
<div class="locations-form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
        <?= $form->field($model, 'description')->textInput(['maxlength' => 255]) ?>
        <?= $form->field($model, 'latitude')->textInput(['maxlength' => 255]) ?>
        <?= $form->field($model, 'longitude')->textInput(['maxlength' => 255]) ?>

    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- locations-form -->
